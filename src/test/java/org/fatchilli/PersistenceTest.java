package org.fatchilli;

import javax.persistence.EntityExistsException;
import javax.persistence.Persistence;

import org.assertj.core.api.Assertions;
import org.fatchilli.model.Susers;
import org.fatchilli.persistence.PersistenceEntityManager;
import org.fatchilli.persistence.SusersPersistence;
import org.junit.jupiter.api.Test;

public class PersistenceTest {
    private final SusersPersistence susersPersistence = new SusersPersistence(
         Persistence.createEntityManagerFactory("avi").createEntityManager());

    @Test
    public void saveAndLoadTest() {
        susersPersistence.save(1, "KK", "Karel");
        susersPersistence.save(2, "TT", "Tom");
        susersPersistence.save(3, "LL", "Lom");

        Assertions.assertThat(susersPersistence.get(1).orElse(null))
                .isNotNull()
                .returns(1L, Susers::getUserId)
                .returns("KK", Susers::getUserGuid)
                .returns("Karel", Susers::getUserName);

        Assertions.assertThat(susersPersistence.getAll()).hasSize(3);

        Assertions.assertThatThrownBy(() -> susersPersistence.save(3, "LL", "Lom")).isInstanceOf(EntityExistsException.class);

        susersPersistence.deleteAll();
        Assertions.assertThat(susersPersistence.getAll()).hasSize(0);

    }
}
