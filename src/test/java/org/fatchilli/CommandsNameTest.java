package org.fatchilli;

import org.assertj.core.api.Assertions;
import org.fatchilli.commands.DeleteAllCommand;
import org.fatchilli.commands.ExitCommand;
import org.fatchilli.commands.PrintAllCommand;
import org.fatchilli.commands.SaveCommand;
import org.junit.jupiter.api.Test;

public class CommandsNameTest {
    @Test
    public void saveCommandNameTest() {
        SaveCommand saveCommand = new SaveCommand();
        Assertions.assertThat(saveCommand.match("Add(1,\"a1\",\"Robert\")")).isTrue();
        Assertions.assertThat(saveCommand.match("Save(2,\"a2\",\"Martin\")")).isFalse();
        Assertions.assertThat(saveCommand.match("Add(\"1\",\"a1\",\"Robert\")")).isFalse();
        Assertions.assertThat(saveCommand.match("Add(\"1\"")).isFalse();
    }

    @Test
    public void printAllCommandNameTest() {
        PrintAllCommand printAllCommand = new PrintAllCommand();
        Assertions.assertThat(printAllCommand.match("PrintAll")).isTrue();
        Assertions.assertThat(printAllCommand.match("Print")).isFalse();
    }

    @Test
    public void deleteAllCommandNameTest() {
        DeleteAllCommand deleteAllCommand = new DeleteAllCommand();
        Assertions.assertThat(deleteAllCommand.match("DeleteAll")).isTrue();
        Assertions.assertThat(deleteAllCommand.match("Delete")).isFalse();
    }

    @Test
    public void exitCommandNameTest() {
        ExitCommand deleteAllCommand = new ExitCommand();
        Assertions.assertThat(deleteAllCommand.match("Exit")).isTrue();
    }
}
