package org.fatchilli;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import javax.persistence.Persistence;

import org.assertj.core.api.Assertions;
import org.fatchilli.commands.DeleteAllCommand;
import org.fatchilli.commands.PrintAllCommand;
import org.fatchilli.commands.SaveCommand;
import org.fatchilli.model.Susers;
import org.fatchilli.persistence.SusersPersistence;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CommandsProcessTest {
    private SusersPersistence susersPersistence;

    @BeforeEach
    public void init() {
        susersPersistence = new SusersPersistence(Persistence.createEntityManagerFactory("avi").createEntityManager());
    }

    @AfterEach
    public void cleanUp() {
        susersPersistence.deleteAll();
    }

    @Test
    public void saveCommandTest() {
        SaveCommand saveCommand = new SaveCommand();
        saveCommand.execute("Add(184,\"aaa\",\"Test\")");

        Assertions.assertThat(susersPersistence.get(184).orElse(null))
                .isNotNull()
                .returns(184L, Susers::getUserId)
                .returns("aaa", Susers::getUserGuid)
                .returns("Test", Susers::getUserName);
    }

    @Test
    public void printAllCommandTest() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        susersPersistence.save(10, "KKK", "Karel");
        susersPersistence.save(20, "TTT", "Tom");
        PrintAllCommand printAllCommand = new PrintAllCommand();
        printAllCommand.execute("PrintAll");

        Assertions.assertThat(outContent.toString()).isEqualToNormalizingNewlines(
                "Susers(userId=10, userGuid=KKK, userName=Karel)\n"
                + "Susers(userId=20, userGuid=TTT, userName=Tom)\n");
    }

    @Test
    public void deleteAllCommandTest() {
        susersPersistence.save(1, "KK", "Karel");
        susersPersistence.save(2, "TT", "Tom");

        Assertions.assertThat(susersPersistence.getAll()).hasSize(2);

        DeleteAllCommand deleteAllCommand = new DeleteAllCommand();
        deleteAllCommand.execute("DeleteAll");

        Assertions.assertThat(susersPersistence.getAll()).hasSize(0);
    }


}
