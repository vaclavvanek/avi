package org.fatchilli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.fatchilli.commands.DeleteAllCommand;
import org.fatchilli.commands.ExitCommand;
import org.fatchilli.commands.InputCommand;
import org.fatchilli.commands.PrintAllCommand;
import org.fatchilli.commands.SaveCommand;

public class App {
    private static final List<InputCommand> commandList =
            Stream.of(new SaveCommand(), new PrintAllCommand(), new DeleteAllCommand(), new ExitCommand())
                    .collect(Collectors.toList());

    public static void main(String[] args) throws IOException {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            String command = reader.readLine();
            try {
                InputCommand inputCommand = commandList.stream()
                        .filter(ic -> ic.match(command))
                        .findFirst()
                        .orElseThrow(IllegalArgumentException::new);
                executor.submit(() -> inputCommand.execute(command));
            } catch (IllegalArgumentException e) {
                System.err.println("Not valid command");
            } catch (Exception e) {
                System.err.println(e.getMessage());
                e.printStackTrace();
            }

        }

    }
}
