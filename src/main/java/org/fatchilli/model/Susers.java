package org.fatchilli.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "SUSERS")
public class Susers implements Serializable {
    @Id
    @EqualsAndHashCode.Include
    @Column(name = "USER_ID")
    private Long userId;

    @Column(name = "USER_GUID")
    private String userGuid;

    @Column(name = "USER_NAME")
    private String userName;
}
