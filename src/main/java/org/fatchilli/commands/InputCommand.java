package org.fatchilli.commands;

public interface InputCommand {
    boolean match(String command);

    void execute(String command);
}
