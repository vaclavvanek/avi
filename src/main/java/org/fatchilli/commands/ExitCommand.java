package org.fatchilli.commands;

public class ExitCommand implements InputCommand {
    private final String command = "Exit";

    @Override
    public boolean match(String command) {
        return this.command.equals(command);
    }

    @Override
    public void execute(String command) {
        System.exit(0);
    }
}
