package org.fatchilli.commands;

import org.fatchilli.persistence.PersistenceEntityManager;
import org.fatchilli.persistence.SusersPersistence;

public class DeleteAllCommand implements InputCommand {
    private static final SusersPersistence susersPersistence = new SusersPersistence(
            PersistenceEntityManager.getEntityManager());

    private final String command = "DeleteAll";

    @Override
    public boolean match(String command) {
        return this.command.equals(command);
    }

    @Override
    public void execute(String command) {
        if (match(command)) {
            susersPersistence.deleteAll();
        }
    }
}
