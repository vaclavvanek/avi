package org.fatchilli.commands;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Persistence;

import org.fatchilli.persistence.PersistenceEntityManager;
import org.fatchilli.persistence.SusersPersistence;

public class SaveCommand implements InputCommand {
    private static final SusersPersistence susersPersistence = new SusersPersistence(
            PersistenceEntityManager.getEntityManager());

    private final Pattern pattern = Pattern.compile(
            "Add\\((?<id>\\d+),\"(?<guid>\\S+)\",*\"(?<name>\\S+)\"\\)");
    @Override
    public boolean match(String command) {
        return pattern.matcher(command).matches();
    }

    @Override
    public void execute(String command) {
        Matcher matcher = pattern.matcher(command);
        if (matcher.matches()) {
            susersPersistence.save(Long.parseLong(matcher.group("id")), matcher.group("guid"), matcher.group("name"));
        }
    }
}
