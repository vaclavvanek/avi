package org.fatchilli.persistence;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface Persistence<T extends Serializable > {
    Optional<T> get(long id);

    List<T> getAll();

    void deleteAll();

    void save(T t);

    void delete(T t);

}
