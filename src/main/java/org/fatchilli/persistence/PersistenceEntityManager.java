package org.fatchilli.persistence;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class PersistenceEntityManager {
    private static volatile EntityManager em;

    public static EntityManager getEntityManager() {
        if (em == null) {
            synchronized (PersistenceEntityManager.class) {
                if (em == null) {
                    em = Persistence.createEntityManagerFactory("avi").createEntityManager();
                }
            }
        }
        return em;
    }
}
