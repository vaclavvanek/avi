package org.fatchilli.persistence;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.fatchilli.model.Susers;

public class SusersPersistence implements Persistence<Susers> {

    private final EntityManager em;

    public SusersPersistence(EntityManager em) {
        this.em = em;
    }

    @Override
    public Optional<Susers> get(long id) {
        return Optional.ofNullable(em.find(Susers.class, id));
    }

    @Override
    public List<Susers> getAll() {
        return em.createQuery("SELECT s FROM Susers s", Susers.class).getResultList();
    }

    @Override
    public void deleteAll() {
        inTransaction(em -> em.createQuery("DELETE FROM Susers s").executeUpdate());
    }

    public Susers save(long id, String userGuid, String userName) {
        Susers susers = new Susers();
        susers.setUserId(id);
        susers.setUserGuid(userGuid);
        susers.setUserName(userName);

        this.save(susers);
        return susers;
    }

    @Override
    public void save(Susers susers) {
        inTransaction(em -> em.persist(susers));
    }

    @Override
    public void delete(Susers susers) {
        em.remove(susers);
    }

    private void inTransaction(Consumer<EntityManager> action) {
        final EntityTransaction transaction = em.getTransaction();
        try {
            transaction.begin();
            action.accept(em);
            transaction.commit();
        }
        catch (RuntimeException e) {
            transaction.rollback();
            throw e;
        }
    }

}
